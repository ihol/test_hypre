/*
 * hdf5rw.hpp
 *
 *  Created on: Jul 6, 2023
 *      Author: ihol
 */

#ifndef HDF5RW_HPP_
#define HDF5RW_HPP_
#include "hdf5.h"

herr_t readHDF5file(const char *fname, int *indx, int *n, int *nnz, int **irn, int **jcn, double **val, double **rhs, int *blockSize);

template<typename scalar_t>
herr_t HDF5readSData(hid_t fid, const char* iname, const int n, scalar_t *val);
template<typename scalar_t>
herr_t HDF5readVData(hid_t fid, const char* name, const int n, scalar_t **val_);




#endif /* HDF5RW_HPP_ */
