#include <iostream>
#include <unistd.h>
#include <algorithm>
#include <string>
#include <vector>
#include <hdf5.h>
#include <cmath>

#include "HYPRE_krylov.h"
#include "HYPRE.h"
#include "HYPRE_parcsr_ls.h"

#include "hdf5rw.hpp"

double getResidual(int  n,   int nnz,
            int *irn, int *jcn,
            double *val,
            double *x,
            double *rhs,
            int indexing)
{
  int  i;
  double rnorm=0.0, bnorm=0.0, rdum;
  double *res = new double[n];

  for (int i=0; i<n; i++){
    res[i] = 0.0;
  }

  // calculate matrix-vector product A*x
  for (int i=0; i<nnz; i++){
    res[irn[i] - indexing] += val[i]*x[jcn[i] - indexing];
  }

  for (int i=0; i<n; i++){
          bnorm+=rhs[i]*rhs[i];
          rnorm+=(rhs[i]-res[i])*(rhs[i]-res[i]);
  }

  delete [] res;
  return sqrt(rnorm)/sqrt(bnorm);
}


int main(int argc, char* argv[])
{
  int            thread_level, pe, npe;
  int            n, nnz;
  int            *irn, *jcn, *rows;
  double         *val, *rhs, *sol;
  int blockSize = 1;
 
  const char     *fname;
  MPI_Comm       comm;
  int indexing;
  
  HYPRE_IJMatrix A;
  HYPRE_ParCSRMatrix parcsr_A;
  HYPRE_IJVector b;
  HYPRE_ParVector par_b;
  HYPRE_IJVector x;
  HYPRE_ParVector par_x;
  HYPRE_Solver solver, precond;  
  

  std::string f(argv[1]);
  fname=f.c_str();
  std::cout<<fname<<std::endl;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &thread_level);
  MPI_Comm_rank(MPI_COMM_WORLD, &pe);
  MPI_Comm_size(MPI_COMM_WORLD, &npe);

  if( pe == 0 ) {
    //readHDF5( fname, &irn, &jcn, &val, &rhs, &n, &nnz, &indexing);
    readHDF5file(fname, &indexing, &n, &nnz, &irn, &jcn, &val, &rhs, &blockSize);
    std::cout<<" ===Sparse matrix info==="<<std::endl;
    std::cout<<" Matrix rank (number of uknowns): "<< n <<"\n"<<" Number of nonzero entries: "<< nnz<<std::endl;
    
    std::vector<double> column_scaling(n,1e-20);
    int j;    
    for (int i = 0; i < nnz; i++)
    {
      j = jcn[i] - indexing;
      column_scaling[j] = std::max(column_scaling[j], std::abs(val[i]));
    }
    for (int i = 0; i < nnz; i++)
    {
      j = jcn[i] - indexing;
      val[i]/=column_scaling[j];
    }
  }
  //return 0;
 
  
  int ilower, iupper, local_size;
  
  ilower = 0;
  iupper = n - 1;
  local_size = iupper - ilower + 1;
  rows = new int[local_size];
  for (int i=0; i<local_size; i++){rows[i] = ilower + i;}

   /* Create the matrix.
      Note that this is a square matrix, so we indicate the row partition
      size twice (since number of rows = number of cols) */
  HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, ilower, iupper, &A);

   /* Choose a parallel csr format storage (see the User's Manual) */
  HYPRE_IJMatrixSetObjectType(A, HYPRE_PARCSR);

   /* Initialize before setting coefficients */
  HYPRE_IJMatrixInitialize(A);
 
  //int* ColPerRow = new int[n];
  //for (int i = 0; i < nnz; i++) {
  //  ColPerRow[irn[i]] += 1;
  //}
  //HYPRE_IJMatrixSetValues(A, n, ColPerRow, irn, jcn, val); // for some reason crashes
  
  int int1 = 1;
  for (int i = 0; i<nnz; i++){
    if (val[i]!=0){
      HYPRE_IJMatrixSetValues(A, 1, &int1, &irn[i], &jcn[i], &val[i]);
    }
  }

  
  HYPRE_IJMatrixAssemble(A);
   /* Get the parcsr matrix object to use */
  HYPRE_IJMatrixGetObject(A, (void**) &parcsr_A);
  
  
   /* Create the rhs and solution */
  HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper,&b);
  HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
  HYPRE_IJVectorInitialize(b);

  HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper,&x);
  HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
  HYPRE_IJVectorInitialize(x);
  
  sol = new double[n];
  for (int i=0; i<n; i++) {sol[i] = 0;}
  
  HYPRE_IJVectorSetValues(b, local_size, rows, rhs);
  HYPRE_IJVectorSetValues(x, local_size, rows, sol);
  
  HYPRE_IJVectorAssemble(b);
  HYPRE_IJVectorGetObject(b, (void **) &par_b);

  HYPRE_IJVectorAssemble(x);
  HYPRE_IJVectorGetObject(x, (void **) &par_x);
  
  int num_iterations;
  double final_res_norm;

  /* Create solver */
  HYPRE_BoomerAMGCreate(&solver);
  HYPRE_BoomerAMGSetPrintLevel(solver, 3);
  HYPRE_BoomerAMGSetTol(solver, 1e-6);      /* conv. tolerance */
  //HYPRE_BoomerAMGSetNumFunctions(solver, 24);
  //int ndof = 4;
  //HYPRE_BoomerAMGSetDofFunc(solver, &ndof); // ndof
  
  HYPRE_BoomerAMGSetup(solver, parcsr_A, par_b, par_x);
  HYPRE_BoomerAMGSolve(solver, parcsr_A, par_b, par_x);
  
  HYPRE_IJVectorGetValues(x, local_size, rows, sol);

  std::cout<<"Solution "<<sol[0]<<" "<<sol[n-1]<<std::endl;
  
  //HYPRE_IJMatrixDestroy(A);
  //HYPRE_IJVectorDestroy(b);
  //HYPRE_IJVectorDestroy(x);
  
  //HYPRE_BoomerAMGDestroy(solver);
  //HYPRE_Finalize();
  
  if (pe == 0) {
    delete [](irn);
    delete [](jcn);
    delete [](val);
    delete [](rhs);
    
    //readHDF5(fname,&irn,&jcn,&val,&rhs,&n,&nnz,&indexing);
    readHDF5file(fname, &indexing, &n, &nnz, &irn, &jcn, &val, &rhs, &blockSize);

    std::cout<<"Calculated residual: "<<
              getResidual(n,nnz,irn,jcn,val,sol,rhs,indexing)<<std::endl;
              
    delete [](irn);
    delete [](jcn);
    delete [](val);
    delete [](rhs);      
    delete [](sol);      
  }  
  

  MPI_Finalize();

  usleep(1000000);

  return 0;
}
