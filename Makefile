CMD := test_hypre

FC = /usr/bin/mpif90
CC = /usr/bin/mpicc
CXX = /usr/bin/mpicxx

FLAGS += -g -O2 -fopenmp -mcmodel=large
FFLAGS := -cpp -std=gnu

FFLAGS += -ffree-line-length-1028 -fdefault-real-8 -fdefault-double-8 -w
FLAGS += -Wunused-parameter -Wunused-variable -Wall -g -fbacktrace
FFLAGS += -Wunused-dummy-argument -Wall

CXXFLAGS= -std=c++14
LDFLAGS= -lstdc++
LIB += -lpthread -lm -ldl -lgomp -lmpi_cxx -lstdc++

HYPRE_HOME = /home/ihol/bin/gnu_openmpi/hypre/install
HYPRE_LIB = -L$(HYPRE_HOME)/lib64 -lHYPRE  -lm -lrt
HYPRE_INC = -I$(HYPRE_HOME)/include

HDF5_HOME=/home/ihol/bin/hdf5-1.12.2-gnu/hdf5
HDF5INC = -I${HDF5_HOME}/include
HDF5LIB = -L${HDF5_HOME}/lib -lhdf5_hl -lhdf5hl_fortran -lhdf5 -lhdf5_fortran

MKLLIB = -L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_openmpi_lp64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core
MKLINC = -I${MKLROOT}/include

LIB += $(MKLLIB) $(HYPRE_LIB) $(HDF5LIB)
INC += $(HDF5INC) $(MKLINC) $(HYPRE_INC)


OBJ= hypre.o hdf5rw.o

all: ${OBJ}
	$(CC) $(OBJ) $(CXXFLAGS) $(LDFFLAGS) $(LIB) -o $(CMD) $(LIB)
	$(RM) *.o *.mod

test_hypre: hypre.o
	$(CC) hypre.o $(CXXFLAGS) $(LDFFLAGS) $(LIB) -o test_hypre $(LIB)

hypre.o: hypre.cpp
	$(CXX) $(CXXFLAGS) $(INC) -c hypre.cpp

hdf5rw.o: hdf5rw.cpp
	$(CXX) $(FLAGS) $(CXXFLAGS) $(INC) -c $< -o hdf5rw.o
	

clean:
	rm -f *.o *.mod *~ $(CMD)
