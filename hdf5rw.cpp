#include "hdf5rw.hpp"
#include <iostream>

herr_t readHDF5file(const char *fname, int *indx, int *n_, int *nnz_, int **irn_,
		int **jcn_, double **val_, double **rhs_, int *blockSize){
	std::cout<<"Reading file"<<std::endl;
	hid_t fid;
	herr_t status;
	int nl, ng;
	int nnz = *nnz_;

	fid = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

	if (H5Lexists(fid, "indexing", H5P_DEFAULT)){
      status = HDF5readSData<int>(fid, "indexing", 1, indx);
    }

	if (H5Lexists(fid, "n", H5P_DEFAULT)){
	  status = HDF5readSData<int>(fid, "n", 1, &ng); if (status) return status;
//      HDF5_integer_reading(fid,ng,"n");
      *n_ = nl = ng;
      //std::cout<<"n "<<nl<<std::endl;
    }
	else
	{
	  if (H5Lexists(fid, "nl", H5P_DEFAULT)){
	    status = HDF5readSData<int>(fid, "nl", 1, &nl);
	    *n_ = nl;
	  }
	  if (H5Lexists(fid, "ng", H5P_DEFAULT)){
	    status = HDF5readSData<int>(fid, "ng", 1, &ng);
	  }
	}

	if (H5Lexists(fid, "nnz", H5P_DEFAULT)){
      status = HDF5readSData<int>(fid, "nnz", 1, &nnz);
      *nnz_ = nnz;
    }
    std::cout<<"nnz "<<nnz<<" n "<<*n_<<" nl "<<nl<<" ng "<<ng<<std::endl;
	
	if (H5Lexists(fid, "block_size", H5P_DEFAULT)){
      status = HDF5readSData<int>(fid, "block_size", 1, blockSize);
    }else{*blockSize = 1;}

	if (H5Lexists(fid, "irn", H5P_DEFAULT)){
      status = HDF5readVData<int>(fid, "irn", nnz, irn_); if (status) return status;
    }

	if ((bool)H5Lexists(fid, "jcn", H5P_DEFAULT)){
      status = HDF5readVData<int>(fid, "jcn", nnz, jcn_);
    }

	if ((bool)H5Lexists(fid, "val", H5P_DEFAULT)){
      status = HDF5readVData<double>(fid, "val", nnz, val_);
    }

 // if necessary convert to zero-based indexing
	if (*indx != 0){
		for(int i = 0; i < nnz; i++){
			(*irn_)[i] -= *indx;
			(*jcn_)[i] -= *indx;
		}
		*indx = 0;
	}
	std::cout<<"index base "<<*indx<<std::endl;

	if ((bool)H5Lexists(fid, "rhs", H5P_DEFAULT)){
      status = HDF5readVData<double>(fid, "rhs", nl, rhs_);
    }
	
	if (!(bool)H5Lexists(fid, "rhs", H5P_DEFAULT) || status<0){
		std::cout<<"setting RHS to 1;"<<std::endl;
		double *data = (double*)malloc(nl * sizeof(double));
		for (int i = 0; i<nl; i++){ data[i] = 1.0; }
		*rhs_ = data;
	}

	status = H5Fclose(fid);
	std::cout<<"finish reading file"<<std::endl;
	return status;
}


template<typename scalar_t>
herr_t HDF5readSData(hid_t fid, const char* name, const int n, scalar_t *val_){
	herr_t status;
	hid_t dataset, dataspace, memspace;
	hid_t datatype;
	hsize_t dimsm[1];
//	size_t  size;

	dimsm[0] = n;
	dataset = H5Dopen(fid, name, H5P_DEFAULT);
	datatype = H5Dget_type(dataset); /* datatype handle */
//	size = H5Tget_size(datatype);
//	printf(" Data size is %d %d \n", (int)size, (int)H5Tget_size(H5T_NATIVE_INT));

	dataspace = H5Dget_space(dataset);
//    status_n  = H5Sget_simple_extent_dims(dataspace, dims_out, NULL);
//    std::cout<<"status_n: "<<status_n<<std::endl;
//    printf("rank %d, dimension %lu \n", n, (unsigned long)(dims_out[0]));

	memspace = H5Screate_simple(1, dimsm, NULL);
	status = H5Dread(dataset, datatype, memspace, dataspace, H5P_DEFAULT, val_);

	return status;
}

template<typename scalar_t>
herr_t HDF5readVData(hid_t fid, const char* name, const int n, scalar_t **val_){
	herr_t status;
	hid_t dataset, dataspace;
	hid_t datatype;
//	size_t  size;
	hsize_t dims[3];

	dataset = H5Dopen(fid, name, H5P_DEFAULT);

	datatype = H5Dget_type(dataset); /* datatype handle */
//	size = H5Tget_size(datatype);
	//printf(" Data size is %d %d \n", (int)size, (int)H5Tget_size(H5T_NATIVE_INT));

	dataspace = H5Dget_space(dataset);
    int ndims = H5Sget_simple_extent_dims(dataspace, dims, NULL);
    //printf("%s: ndims = %d, dims[0] = %d \n", name, ndims, int(dims[0]));
    if (ndims != 1) {
		//std::cout<<name<<" "<<dims[0]<<" "<<dims[1]<<std::endl;
		//std::cout<<"Error: Expected 1-dimensional dataset "<<name<<std::endl;
		status = -1;
        H5Sclose(dataspace);
        H5Dclose(dataset);
        //H5Fclose(fid);
        //exit(1);		
    }else{
        scalar_t *data = (scalar_t*)malloc(dims[0] * sizeof(scalar_t));
	    //scalar_t *data = new scalar_t(dims[0]);
        status = H5Dread(dataset, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
        *val_ = data;
	}
	return status;
}


